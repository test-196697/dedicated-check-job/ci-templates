#!/usr/bin/env sh

usage() {
  echo "Usage: ./check_scan_duration.sh REPORT_PATH [-m MAX_SCAN_DURATION_SECOND [-p SCAN_DURATION_MARGIN_PERCENT]]"
}

report=$1
max=
margin=

if [ -z "$report" ]; then
  usage
  exit 1
fi

OPTIND=2

while getopts ":m:p:" opt; do
  case "$opt" in
    m)
      max=${OPTARG}
      ;;
    p)
      margin=${OPTARG}
      ;;
    *) ;;

  esac
done
shift $((OPTIND - 1))

# validate that args were supplied properly
# and we didn't gobble up the subsequent opt
if [ "${max:0:1}" == "-" ]; then
  max=
  margin=
fi

if [ "${margin:0:1}" == "-" ]; then
  margin=
fi

if [ -n "$margin" ]; then
  max=$((max * (100 + margin) / 100))
fi

start_time=$(jq -r '.scan.start_time' "$report")
end_time=$(jq -r '.scan.end_time' "$report")

if [ "$start_time" == "null" ]; then
  echo "Error processing $report (expected .scan.start_time to be present)"
  exit 1
fi

if [ "$end_time" == "null" ]; then
  echo "Error processing $report (expected .scan.end_time to be present)"
  exit 1
fi

date_format="%Y-%m-%dT%H:%M:%S"
start_time=$(date -D "$date_format" --date="$start_time" +"%s")
end_time=$(date -D "$date_format" --date="$end_time" +"%s")
duration="$((end_time - start_time))"

echo -n "Scan duration: $duration(s) "

# if max not set, just report on scan duration and exit
if [ -z "$max" ]; then
  echo
  exit 0
fi

if [ "$duration" -gt "$max" ]; then
  echo "exceeds maximum (max: $max(s))"
  exit 1
else
  echo "does not exceed maximum $max(s)"
fi
